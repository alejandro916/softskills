﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class StartMenu : ScriptableObject
{
    public int Players;
    public float Time;
    public int cam;
}
