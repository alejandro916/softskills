﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Images : ScriptableObject
{
    public Sprite[] sprites;

    public Sprite getSprite(string name)
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name.Contains(name))
            {
                return sprites[i];
            }
        }
        return null;
    }
}
