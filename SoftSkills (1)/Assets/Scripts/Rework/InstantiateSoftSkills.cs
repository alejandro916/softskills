﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Esta struct se utiliza para sustituir la classe SelectOption y ahorarse el switch
//Permite asocair cada boton con una imagen

[System.Serializable]
public struct SkillButton {
    public Button button;
    public Sprite image;
}

public class InstantiateSoftSkills : MonoBehaviour
{

    [Header("Skill Buttons")]
    [SerializeField] private SkillButton[] skillButtons;

    [Header("UI Panels")]
    [SerializeField] private GameObject firstMenu;
    [SerializeField] private GameObject secondMenu;
    [SerializeField] private GameObject playerMenu;


    [Header("UI Buttons")]
    [SerializeField] private Button backgroundButton;
    [SerializeField] private Button backButton;
    [SerializeField] private Button addButton;
    [SerializeField] private Button removeButton;
    [SerializeField] private Button playersButton;
    [SerializeField] private Button[] numPlayersButtons;

    [Space(20)]
    [SerializeField] private int numberOfPlayers;
    [SerializeField] private StartMenu SM;
    [SerializeField] private Image imageContainer;
    [SerializeField] private Camera cam;
    [SerializeField] private Text playerText;

    private Animator firstMenuAnimator;
    private Animator secondMenuAnimator;

    void Start() {

        SetMenusVisibility(false, false);
        playerMenu.SetActive(false);

        firstMenuAnimator = firstMenu.GetComponent<Animator>();

        SetOnclickListeners();
    }
    
    private void SetOnclickListeners() {

        foreach (SkillButton skill in skillButtons) {

            skill.button.onClick.AddListener(() => ChangeImageContainer(skill.image));

            //Ajusta los bordes de los botones para que conicidan con los bordes de los sprites
            if (skill.button.TryGetComponent<Image>(out Image image)) {

                image.alphaHitTestMinimumThreshold = 0.001f;
            }
        }

        for(int i = 0; i < 4; i++) {

            numPlayersButtons[i].onClick.AddListener(() => ChangePlayer(i));
            Debug.Log(i);
        }

        backgroundButton.onClick.AddListener(() => ShowSkillsMenu());
        backButton.onClick.AddListener(() => SetMenusVisibility(true, false));
        addButton.onClick.AddListener(() => SetMenusVisibility(true, false));
        removeButton.onClick.AddListener(() => SetMenusVisibility(true, false));
        playersButton.onClick.AddListener(() => ShowPlayersMenu());
    }

    private void ChangeImageContainer(Sprite image) {

        SetMenusVisibility(false, true);
        imageContainer.sprite = image;
        Debug.Log(imageContainer.sprite);
    }

    private void SetMenusVisibility(bool first, bool second) {

        firstMenu.SetActive(first);
        secondMenu.SetActive(second);

        if (firstMenu) {
            //TODO: play first menu animation
        }
    }

    #region Button actions

    private void ShowPlayersMenu() {

        playerMenu.SetActive(true);
        SetMenusVisibility(false, false);
        Animator animator = playerMenu.GetComponent<Animator>();
        animator.SetInteger("Players", numberOfPlayers);
    }

    private void ShowSkillsMenu() {

        SetMenusVisibility(!firstMenu.activeSelf & !secondMenu.activeSelf & !playerMenu.activeSelf, false);
        Vector3 ScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 90);
        firstMenu.transform.position = cam.ScreenToWorldPoint(ScreenPoint);
        secondMenu.transform.position = cam.ScreenToWorldPoint(ScreenPoint);
        firstMenuAnimator.SetTrigger("Open");

        //Limita la posición del menu para que no se salga de la pantalla
        Vector3 menuPos = firstMenu.transform.position;
        menuPos = new Vector3(menuPos.x, Mathf.Clamp(menuPos.y, -23.24f, 36.50f), menuPos.z);
        menuPos = new Vector3(Mathf.Clamp(menuPos.x, -75.6f, 76.14f), menuPos.y, menuPos.z);
        firstMenu.transform.position = secondMenu.transform.position = menuPos;
    }

    private void ChangePlayer(int playerNum) {

        playerMenu.SetActive(false);
        SetMenusVisibility(true, false);
        playerText.text = "P" + playerNum;
    }

    #endregion
}
