﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackwardButton : MonoBehaviour
{
    public Text text;//s'utilitza per a esborrar el text que indica el jugador al que estem aplicant el canvi de puntuació
    public Image imageComponent;//utilitzat per poder habilitar i deshabilitar la imatge
    public GameObject FirstMenu, AlertBoxContainer;
    private GameObject SecondMenu, Canvas;

    private void Start()
    {
        SecondMenu = GameObject.Find("SecondMenuSprites(Clone)");
        Canvas = GameObject.Find("Canvas");
    }
    private void OnMouseDown()
    {
        if (!AlertBoxContainer.activeSelf)
        {
            imageComponent = GameObject.Find("SpriteMenu2").GetComponent<Image>();
            imageComponent.enabled = false;
            //text.enabled = true;
            Instantiate(FirstMenu, new Vector3(Canvas.GetComponent<GetClickPosition>().posX, Canvas.GetComponent<GetClickPosition>().posY, Canvas.GetComponent<GetClickPosition>().posZ), Quaternion.identity);
            FirstMenu.SetActive(true);
            SecondMenu.SetActive(false);
        }
    }
}
