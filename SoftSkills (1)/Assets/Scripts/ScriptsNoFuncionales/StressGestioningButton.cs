﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StressGestioningButton : MonoBehaviour
{
    public Text text;//s'utilitza per a esborrar el text que indica el jugador al que estem aplicant el canvi de puntuació
    public Image imageComponent;//utilitzat per poder habilitar i deshabilitar la imatge
    public GameObject AlertBoxContainer;
    private GameObject FirstMenu, Canvas;
    public GameObject SecondMenuSprites;
    public Images arrayImatges;

    private void Start()
    {
        FirstMenu = GameObject.Find("FirstMenuSprites(Clone)");
        Canvas = GameObject.Find("Canvas");
        
    }
    private void OnMouseDown()
    {
       
        if (!AlertBoxContainer.activeSelf)
        {
            Instantiate(SecondMenuSprites, new Vector3(Canvas.GetComponent<GetClickPosition>().posX, Canvas.GetComponent<GetClickPosition>().posY, Canvas.GetComponent<GetClickPosition>().posZ), Quaternion.identity);
            // text.enabled = false;
            imageComponent = GameObject.Find("SpriteMenu2").GetComponent<Image>();
            imageComponent.enabled = true;
            imageComponent = SecondMenuSprites.transform.GetChild(4).transform.GetChild(0).gameObject.GetComponent<Image>();
            imageComponent.rectTransform.sizeDelta = new Vector2(27.74f, 32.43f);

            Debug.Log(this.name);
            switch (this.name)
            {
                case "stressIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("stress");
                    break;
                case "pensamientoCriticoIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("pensamiento");
                    break;
                case "compromisoIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("compromiso");
                    break;
                case "investigationIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("investigation");
                    break;
                case "tiemGestionBtn":
                    imageComponent.sprite = arrayImatges.getSprite("time");
                    break;
                case "newTechIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("newtech");
                    break;
                case "comunicationIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("comunication");
                    break;
                case "teamworkIconBtn":
                    imageComponent.sprite = arrayImatges.getSprite("teamwork");
                    break;
            }
            
            FirstMenu.SetActive(false);
            SecondMenuSprites.SetActive(true);
        }
    }
}
