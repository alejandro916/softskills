﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoldingButton : MonoBehaviour
{
    bool HoldButton;
    double StartTime;
    public GameObject PlayerMenu;
    public GameObject FirstMenu;
    public GameObject SecondMenu;
    public GameObject Center;
    public Text actualPlayer;
    public Image image;
    private Animator animator;
    public int numberOfPlayers;


    // Start is called before the first frame update
    void Start()
    {

        PlayerMenu = GameObject.Find("PlayerMenuSprites");
        HoldButton = false;
        animator = PlayerMenu.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (numberOfPlayers != 1) {
            if (Input.GetMouseButtonUp(0))
            {
                HoldButton = false;
            }
            if ((Time.time - StartTime) > 0.5 && HoldButton)
            {
                image.enabled = false;
                PlayerMenu.SetActive(true);
                Debug.Log("El numero de jugadors es : " + numberOfPlayers);
                animator.SetInteger("Players", numberOfPlayers);
                FirstMenu.SetActive(false);
                SecondMenu.SetActive(false);
                Center.SetActive(false);
                actualPlayer.text = "";
                HoldButton = false;
            }
        }
    }

    private void OnMouseDown()
    {
        HoldButton = true;
        StartTime = Time.time;
    }
    private void OnMouseUp()
    {
        HoldButton = false;
    }
}
