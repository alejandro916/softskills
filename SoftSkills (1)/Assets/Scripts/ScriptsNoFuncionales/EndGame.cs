﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public GameObject Alert, InformationMenu, Compromiso,Capacidad,Dominios,Pensamiento,GesitionTiempo,GestionEstres,Investigacion,Trabajo;
        
    private void OnMouseDown()
    {
        if (InformationMenu.activeSelf)
        {
            Compromiso.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            Capacidad.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            Dominios.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            Pensamiento.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            GesitionTiempo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            GestionEstres.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            Investigacion.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
            Trabajo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.2f);
        }
        
        Alert.SetActive(true);
        Time.timeScale = 0;
    }
}
