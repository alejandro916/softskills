﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeGestioningButton : MonoBehaviour
{
    public Text text;//s'utilitza per a esborrar el text que indica el jugador al que estem aplicant el canvi de puntuació
    public Sprite myImage; //utilitzat per cambiar la imatge del centre a el soft skill seleccionat per l'usuari
    public Image imageComponent;//utilitzat per poder habilitar i deshabilitar la imatge
    public GameObject FirstMenu, AlertBoxContainer;
    private GameObject SecondmenuMenu;

    private void Start()
    {
        imageComponent = GameObject.Find("SpriteMenu2").GetComponent<Image>();
        SecondmenuMenu = GameObject.Find("SecondMenuSprites");
        AlertBoxContainer = GameObject.Find("AlertBoxContainer");
    }
    private void OnMouseDown()
    {
        if (!AlertBoxContainer.activeSelf)
        {
            text.enabled = false;
            imageComponent.enabled = true;
            imageComponent.rectTransform.sizeDelta = new Vector2(64.19f, 68.07f);
            imageComponent.sprite = myImage;
            FirstMenu.SetActive(false);
            SecondmenuMenu.SetActive(true);
        }
    }
}
