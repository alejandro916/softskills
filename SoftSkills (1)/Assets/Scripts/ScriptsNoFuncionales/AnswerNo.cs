﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerNo : MonoBehaviour
{
    public GameObject Alert, Compromiso, Capacidad, Dominios, Pensamiento, GesitionTiempo, GestionEstres, Investigacion, Trabajo;

    private void OnMouseDown()
    {
        Compromiso.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Capacidad.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Dominios.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Pensamiento.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        GesitionTiempo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        GestionEstres.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Investigacion.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Trabajo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Alert.SetActive(false);
        Time.timeScale = 1;
    }
}
