﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectPlayer : MonoBehaviour
{
    public Text playerText;
    public Text ActualPlayerText;
    public GameObject FirstMenu, PlayerMenuSprites, center, AlertBoxContainer;
    
    private void OnMouseDown()
    {
        if (!AlertBoxContainer.activeSelf)
        {
            //center.SetActive(true);
            FirstMenu.SetActive(true);
            PlayerMenuSprites.SetActive(false);
            ActualPlayerText.enabled = true;
            ActualPlayerText.text = playerText.text;
            ActualPlayerText.color = playerText.color;
        }
    }
}
