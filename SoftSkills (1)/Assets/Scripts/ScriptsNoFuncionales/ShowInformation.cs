﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInformation : MonoBehaviour
{
    public GameObject informationMenu, FirstMenu, SecondMenu, PlayerMenu, MainMenu, AlertBoxContainer;
    public Sprite arrow;
    public Sprite information;

    private int menu, prevmenu;

    void OnMouseDown()
    {
        if (!AlertBoxContainer.activeSelf)
        {
            if (FirstMenu.activeSelf)
            {
                menu = 1;
            }
            else if (SecondMenu.activeSelf)
            {
                menu = 2;
            }
            else if (PlayerMenu.activeSelf)
            {
                menu = 3;
            }
            else if (MainMenu.activeSelf)
            {
                menu = 4;
            }
            else if (AlertBoxContainer.activeSelf)
            {
                menu = 5;
            }
            else
            {
                menu = 0;
            }
            
            switch (menu)
            {
                case 0:
                    CheckPrev();
                    break;
                case 1:
                    FirstMenu.SetActive(false);
                    informationMenu.SetActive(true);
                    this.GetComponent<SpriteRenderer>().sprite = arrow;
                    this.transform.localScale = new Vector3(15, 15, 15);
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                    prevmenu = 1;
                    break;
                case 2:
                    SecondMenu.SetActive(false);
                    informationMenu.SetActive(true);
                    this.GetComponent<SpriteRenderer>().sprite = arrow;
                    this.transform.localScale = new Vector3(15, 15, 15);
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                    prevmenu = 2;
                    break;
                case 3:
                    PlayerMenu.SetActive(false);
                    informationMenu.SetActive(true);
                    this.GetComponent<SpriteRenderer>().sprite = arrow;
                    this.transform.localScale = new Vector3(15, 15, 15);
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                    prevmenu = 3;
                    break;
                case 4:
                    MainMenu.SetActive(false);
                    informationMenu.SetActive(true);
                    this.GetComponent<SpriteRenderer>().sprite = arrow;
                    this.transform.localScale = new Vector3(15, 15, 15);
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                    prevmenu = 4;
                    break;
                case 5:
                    AlertBoxContainer.SetActive(false);
                    informationMenu.SetActive(true);
                    this.GetComponent<SpriteRenderer>().sprite = arrow;
                    this.transform.localScale = new Vector3(15, 15, 15);
                    GetComponent<SpriteRenderer>().sortingOrder = 1;
                    prevmenu = 5;
                    break;
            }
        }
    }

    private void CheckPrev()
    {
        switch (prevmenu)
        {
            case 1:
                FirstMenu.SetActive(true);
                informationMenu.SetActive(false);
                this.GetComponent<SpriteRenderer>().sprite = information;
                this.transform.localScale = new Vector3(8, 8, 8);
                GetComponent<SpriteRenderer>().sortingOrder = 0;
                break;
            case 2:
                SecondMenu.SetActive(true);
                informationMenu.SetActive(false);
                this.GetComponent<SpriteRenderer>().sprite = information;
                this.transform.localScale = new Vector3(8, 8, 8);
                GetComponent<SpriteRenderer>().sortingOrder = 0;
                break;
            case 3:
                PlayerMenu.SetActive(true);
                informationMenu.SetActive(false);
                this.GetComponent<SpriteRenderer>().sprite = information;
                this.transform.localScale = new Vector3(8, 8, 8);
                GetComponent<SpriteRenderer>().sortingOrder = 0;
                break;
            case 4:
                MainMenu.SetActive(true);
                informationMenu.SetActive(false);
                this.GetComponent<SpriteRenderer>().sprite = information;
                this.transform.localScale = new Vector3(8, 8, 8);
                GetComponent<SpriteRenderer>().sortingOrder = 0;
                break;
            case 5:
                AlertBoxContainer.SetActive(true);
                informationMenu.SetActive(false);
                this.GetComponent<SpriteRenderer>().sprite = information;
                this.transform.localScale = new Vector3(8, 8, 8);
                GetComponent<SpriteRenderer>().sortingOrder = 0;
                break;
        }
    }
}
