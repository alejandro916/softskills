﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text selectedItemText;
    public GameObject MainMenuObject, PlayerMenu, FirstMenu, PlayerIcon, AlertBoxContainer;
    public Text actualPlayer;

    private void OnMouseDown()
    {
        if (!AlertBoxContainer.activeSelf)
        {
            if (selectedItemText.text.Contains("2"))
            {
                PlayerIcon.GetComponent<HoldingButton>().numberOfPlayers = 2;
                // actualPlayer.text = "P2";
                PlayerMenu.SetActive(true);
                PlayerMenu.GetComponent<Animator>().SetInteger("Players", 2);
                MainMenuObject.SetActive(false);
                PlayerMenu.SetActive(true);
            }
            else if (selectedItemText.text.Contains("3"))
            {
                PlayerIcon.GetComponent<HoldingButton>().numberOfPlayers = 3; ;
                PlayerMenu.SetActive(true);
                PlayerMenu.GetComponent<Animator>().SetInteger("Players", 3);
                MainMenuObject.SetActive(false);
            }
            else if (selectedItemText.text.Contains("4"))
            {
                PlayerIcon.GetComponent<HoldingButton>().numberOfPlayers = 4;
                PlayerMenu.SetActive(true);
                PlayerMenu.GetComponent<Animator>().SetInteger("Players", 4);
                MainMenuObject.SetActive(false);
            }
            else
            {
                PlayerIcon.GetComponent<HoldingButton>().numberOfPlayers = 1;
                FirstMenu.SetActive(true);
                actualPlayer.enabled = true;
                actualPlayer.text = "P1";
                actualPlayer.color = new Color(0.95f, 0.18f, 0.18f, 1);
                MainMenuObject.SetActive(false);
                PlayerIcon.SetActive(true);
            }
        }
    }
}
