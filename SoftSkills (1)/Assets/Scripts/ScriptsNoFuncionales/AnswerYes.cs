﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerYes : MonoBehaviour
{
    public GameObject PlayerIconBTN, Compromiso, Capacidad, Dominios, Pensamiento, GesitionTiempo, GestionEstres, Investigacion, Trabajo;
    public Text ActualPlayerText;
    public GameObject FirstMenu;
    public GameObject SecondMenu;
    public GameObject PlayerMenuSprites;
    public GameObject InformationMenu;
    public GameObject MainMenu;
    public Image imageComponent;//utilitzat per poder habilitar i deshabilitar la imatge
    public GameObject Alert;
    
    private void OnMouseDown()
    {
        
        Compromiso.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Capacidad.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Dominios.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Pensamiento.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        GesitionTiempo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        GestionEstres.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Investigacion.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Trabajo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        InformationMenu.SetActive(false);
        Alert.SetActive(false);
        this.MainMenu.SetActive(true);
        imageComponent.enabled = false;
        PlayerIconBTN.SetActive(false);
        ActualPlayerText.text = "";
        FirstMenu.SetActive(false);
        SecondMenu.SetActive(false);
        PlayerMenuSprites.SetActive(false);

        Time.timeScale = 1;
    }
}
