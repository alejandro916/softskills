﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrevCam : MonoBehaviour
{
    public StartMenu SM;

    private void OnMouseDown()
    {
        SM.cam -= 1;
        if (SM.cam < 0)
        {
            SM.cam = 2;
        }
    }
}
