﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Este script lo unico que hace es que los botones se ajusten a la parte transparente de la imagen

public class ImageTransparentClick : MonoBehaviour
{
    private void OnEnable() {

        if (TryGetComponent<Image>(out Image image)) {

            image.alphaHitTestMinimumThreshold = 0.001f;
        }
    }
}
