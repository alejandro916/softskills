﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResPlayers : MonoBehaviour
{
    public Text players;

    private void OnMouseDown()
    {
        if (!(int.Parse(players.text) <= 0))
        {
            players.text = int.Parse(players.text) - 1 + "";
        }
    }
}
