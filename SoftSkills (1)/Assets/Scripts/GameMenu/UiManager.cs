﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    #region Private variables

    [Header("UI Buttons")]
    [SerializeField] private Button startButton;
    [SerializeField] private Button addPlayerButton;
    [SerializeField] private Button removePlayerButton;
    [SerializeField] private Button addTimeButton;
    [SerializeField] private Button removeTimeButton;

    [Header("UI Texts")]
    [SerializeField] private Text playerNumText;
    [SerializeField] private Text gameDurationText;
    [SerializeField] private Text firstLevelText;
    [SerializeField] private Text serverStatusText;
    [SerializeField] private Text listeningPortText;

    [Header("UI Panels")]
    [SerializeField] private GameObject startMenuPanel;
    [SerializeField] private GameObject statusPanel;
    [SerializeField] private GameObject hudPanel;

    [Header("NetworkManager")]
    [SerializeField] private int minPlayers;
    [SerializeField] private int maxPlayers;
    [SerializeField] private int minTime;
    [SerializeField] private int maxTime;

    [Space(20)]
    private int numPlayers;
    private float gameDuration;
    private bool serverStatus;
    private int listeningPort;

    #endregion

    #region Unity events

    private void Start() {

        AddClickListeners();
        SetDefaultValues();

        SetHudPanelVisibility(false);
    }

    #endregion

    private void AddClickListeners() {

        startButton.onClick.AddListener(StartGame);

        addPlayerButton.onClick.AddListener(() => { ChangeNumPlayers(1); });
        removePlayerButton.onClick.AddListener(() => { ChangeNumPlayers(-1); });

        addTimeButton.onClick.AddListener(() => { ChangeGameDuration(5); });
        removeTimeButton.onClick.AddListener(() => { ChangeGameDuration(-5); });
    }

    private void SetDefaultValues() {

        //TODO: Use network behaivour default values
        numPlayers = 1;
        gameDuration = 60;
        serverStatus = true;
        listeningPort = 7777;

        playerNumText.text = numPlayers.ToString();
        gameDurationText.text = gameDuration.ToString();
        serverStatusText.text = serverStatus ? "ONLINE" : "OFFLINE";
        listeningPortText.text = listeningPort.ToString();
    }

    private void StartGame() {

        SetHudPanelVisibility(true);
        SetStartMenuPanelVisibility(false);
        SetStatusPanelVisibility(false);

        //TODO: Call SceneManager to change scene
    }

    private void ChangeNumPlayers(int difference) {

        numPlayers = Mathf.Clamp((numPlayers + difference), minPlayers, maxPlayers);
        playerNumText.text = numPlayers.ToString();
    }

    private void ChangeGameDuration(int difference) {

        gameDuration = Mathf.Clamp((gameDuration + difference), minTime, maxTime);
        gameDurationText.text = gameDuration.ToString();
    }

    private void SetStartMenuPanelVisibility(bool visibility) {

        startMenuPanel.SetActive(visibility);
    }

    private void SetStatusPanelVisibility(bool visibility) {

        statusPanel.SetActive(visibility);
    }

    private void SetHudPanelVisibility(bool visibility) {

        hudPanel.SetActive(visibility);
    }
}
