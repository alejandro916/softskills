﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextCam : MonoBehaviour
{
    public StartMenu SM;

    private void OnMouseDown()
    {
        SM.cam += 1;
        if (SM.cam > 2)
        {
            SM.cam = 0;
        }
    }
}
