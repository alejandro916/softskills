﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GController : MonoBehaviour
{
    public Text Players, CamName;
    public GameObject Timer;
    public Canvas canvas, CanvasSoftSkills;
    
    public StartMenu SM;


    public Camera[] cameras;

    private float max;

    private void Start()
    {
        SM.Time = 0;
        max = 3600;
        SM.cam = 0;
    }

    private void FixedUpdate()
    {
        if (Timer.activeSelf)
        {
            if (max > 0)
            {
                int minutes = Mathf.FloorToInt(max / 60);
                int seconds = Mathf.FloorToInt(max - minutes * 60);
                string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);
                Timer.transform.GetChild(0).GetComponent<Text>().text = niceTime;
                max -= Time.deltaTime / (SM.Time / 60);
            }
        }
        SM.Players = int.Parse(Players.text);

        if (SM.cam == 0)
        {
            cameras[0].gameObject.SetActive(true);
            cameras[1].gameObject.SetActive(false);
            cameras[2].gameObject.SetActive(false);
            canvas.worldCamera = cameras[0];
            CanvasSoftSkills.worldCamera = cameras[0];
            CamName.text = cameras[0].name;
        } else if (SM.cam == 1)
        {
            cameras[0].gameObject.SetActive(false);
            cameras[1].gameObject.SetActive(true);
            cameras[2].gameObject.SetActive(false);
            canvas.worldCamera = cameras[1];
            CanvasSoftSkills.worldCamera = cameras[1];
            CamName.text = cameras[1].name;
        } else if (SM.cam == 2)
        {
            cameras[0].gameObject.SetActive(false);
            cameras[1].gameObject.SetActive(false);
            cameras[2].gameObject.SetActive(true);
            canvas.worldCamera = cameras[2];
            CanvasSoftSkills.worldCamera = cameras[2];
            CamName.text = cameras[2].name;
        }
    }
}
