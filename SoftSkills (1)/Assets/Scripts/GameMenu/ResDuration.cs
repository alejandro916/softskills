﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResDuration : MonoBehaviour
{
    public Text duration;
    private bool holdbutton;
    private double starttime;
    private int loopactual, loopupdate;

    private void Start()
    {
        loopupdate = 3;
    }

    private void FixedUpdate()
    {
        loopactual++;
        if ((Time.time - starttime) > 0.5f && holdbutton)
        {
            if (loopactual % loopupdate == 0)
            {
                if (!(int.Parse(duration.text) <= 0))
                {
                    duration.text = int.Parse(duration.text) - 1 + "";
                }
                else
                {
                    duration.text = 60 + "";
                }
            }
        }
    }

    private void OnMouseDown()
    {
        holdbutton = true;
        starttime = Time.time;

        if (!(int.Parse(duration.text) <= 0))
        {
            duration.text = int.Parse(duration.text) - 1 + "";
        }
        else
        {
            duration.text = 60 + "";
        }
    }

    private void OnMouseUp()
    {
        holdbutton = false;
    }
}
